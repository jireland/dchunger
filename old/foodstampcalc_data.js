
// Data version - update with each change!  This will be displayed at the bottom of the page.
var CALC_DATA_VERSION = "2012.10.01 V1"; // use whatever naming works for you

// DC Hunger home page
var DC_HUNGER_URL = "http://dchunger.org";
var DC_HUNGER_CALC_URL = ".";

// gross monthly income standards for categorical eligibility (based on 10/01/12 figures)              
var INCOME_STDS = new Array();           
INCOME_STDS[1] = 1862.00;          
INCOME_STDS[2] = 2522.00;          
INCOME_STDS[3] = 3182.00;          
INCOME_STDS[4] = 3842.00;          
INCOME_STDS[5] = 4502.00;          
INCOME_STDS[6] = 5162.00;          
INCOME_STDS[7] = 5822.00;          
INCOME_STDS[8] = 6482.00;          
var MAX_INCOME_STDS_HH = 8;
var ADDITIONAL_INCOME_STD = 660.00;         

// standard deduction (based on 10/01/12 figures)
var STANDARD_DEDUCTIONS = new Array();
STANDARD_DEDUCTIONS[1] = 149;
STANDARD_DEDUCTIONS[2] = 149;
STANDARD_DEDUCTIONS[3] = 149;
STANDARD_DEDUCTIONS[4] = 160;
STANDARD_DEDUCTIONS[5] = 187;
STANDARD_DEDUCTIONS[6] = 214;
STANDARD_DEDUCTIONS[7] = 214;
STANDARD_DEDUCTIONS[8] = 214;
var MAX_ASSIST_UNIT_SIZE = 8;
var MAX_STANDARD_DEDUCTION = 214;

// maximum food stamp allotments for household size (based on 10/01/09 figures)
var MAX_ALLOTMENTS = new Array();
MAX_ALLOTMENTS[1] = 200;
MAX_ALLOTMENTS[2] = 367;
MAX_ALLOTMENTS[3] = 526;
MAX_ALLOTMENTS[4] = 668;
MAX_ALLOTMENTS[5] = 793;
MAX_ALLOTMENTS[6] = 952;
MAX_ALLOTMENTS[7] = 1052;
MAX_ALLOTMENTS[8] = 1202;
var MAX_ALLOTMENTS_HH = 8;
var ADDITIONAL_ALLOTMENT = 150;

// maximum allowable monthly net income standards  100% FPL (based on 10/01/12 figures)               
var MAX_ALLOW_INCOME_STD = new Array();
MAX_ALLOW_INCOME_STD[1] = 931.00;     
MAX_ALLOW_INCOME_STD[2] = 1261.00;          
MAX_ALLOW_INCOME_STD[3] = 1591.00;         
MAX_ALLOW_INCOME_STD[4] = 1921.00;          
MAX_ALLOW_INCOME_STD[5] = 2251.00;          
MAX_ALLOW_INCOME_STD[6] = 2581.00;          
MAX_ALLOW_INCOME_STD[7] = 2911.00;          
MAX_ALLOW_INCOME_STD[8] = 3241.00;          
var MAX_ALLOW_INCOME_STD_HH = 8;
var ADDITIONAL_ALLOW_INCOME_STD = 330.00;            

// medical expenses
var MIN_MEDICAL_EXPENSE = 35;

// standard utility deductions (based on 10/01/12 figures from Rick Walker at IMA)
var UTILITY_HEATING_AND_COOLING = 279.00;
var UTILITY_TWO_UTILITIES = 222.00;
var UTILITY_ONE_UTILITY = 58.00;
var UTILITY_PHONE = 46.00;
var SHELTER_CAP = 459.00;
